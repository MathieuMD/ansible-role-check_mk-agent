Ansible Role Check_MK Agent
===========================

Install and configure [Check_MK Agent](http://mathias-kettner.com/checkmk.html).

Role Variables
--------------

`check_mk_agent_servers` is the list of allowed IP (Xinetd option `only_from`).

Example Playbook
----------------

```yaml
- hosts: all
  roles:
  - role: check_mk-agent
    check_mk_agent_servers: { 10.1.1.10, 10.2.2.10 }
```

License
-------

GPLv3

