---
######################################################################
# Preparation
######################################################################

- include_vars: RedHat.yml
  when: ansible_os_family == 'RedHat'
  tags: [ 'configuration', 'package', 'check_mk-agent' ]

- include_vars: Debian.yml
  when: ansible_os_family == 'Debian'
  tags: [ 'configuration', 'package', 'check_mk-agent' ]

# FIXME TODO
# - name: Gather facts from Check_MK servers
#   setup:
#     delegate_to: "{{ item }}"
#     delefate_facts: yes
#     with_items: "{{ groups['{{ check_mk_agent_servers_group }}'] }}"
#   tags: [ 'configuration', 'check_mk-agent' ]

######################################################################
# Installation
######################################################################

#=====================================================================
# Red Hat
#=====================================================================

- name: Install required packages for SELinux (Red Hat)
  yum: name=libselinux-python state=present
  when: ansible_os_family == 'RedHat'
  tags: [ 'package', 'selinux', 'check_mk-agent' ]

# To use EPEL on RHEL, the "optional" repo is required:
# https://fedoraproject.org/wiki/EPEL#How_can_I_use_these_extra_packages.3F
# Ignore errors because you may use RHN Classic instead Subscription Manager,
# in which case: rhn-channel -u USERID -p PASSWORD --add --channel=rhel-x86_64-server-optional-6
- name: Enable RHEL Optional repo (RHEL)
  command: "subscription-manager repos --enable=rhel-{{ ansible_distribution_major_version }}-server-optional-rpms"
  changed_when: false
  ignore_errors: yes
  when: ansible_distribution == 'RedHat'
  tags: [ 'package', 'check_mk-agent' ]

- name: Install EPEL (RedHat 6+)
  yum: name="https://dl.fedoraproject.org/pub/epel/epel-release-latest-{{ ansible_distribution_major_version }}.noarch.rpm" state=present
  when: ansible_os_family == 'RedHat' and ansible_distribution_major_version >= '6'
  tags: [ 'package', 'check_mk-agent' ]

# On RHEL 5, SSL validation is not available, so we cannot use an HTTPS URL.
- name: Install EPEL (RedHat 5)
  yum: name="http://dl.fedoraproject.org/pub/epel/epel-release-latest-{{ ansible_distribution_major_version }}.noarch.rpm" state=present
  when: ansible_os_family == 'RedHat' and ansible_distribution_major_version == '5'
  tags: [ 'package', 'check_mk-agent' ]

- name: Install the required packages (Red Hat)
  yum: name="{{ item }}" state=present
  with_items:
    - xinetd
    - "{{ check_mk_agent_package }}"
  when: ansible_os_family == 'RedHat'
  notify: restart xinetd
  tags: [ 'package', 'check_mk-agent' ]

#=====================================================================
# Debian
#=====================================================================

# On Debian 8 (Jessie) the package is in jessie-backports
- name: Add backports repository (Debian Jessie)
  apt_repository: repo='deb http://httpredir.debian.org/debian {{ ansible_distribution_release }}-backports main contrib'
                  state=present update_cache=yes
  when: ansible_distribution_release == 'jessie'
  tags: [ 'package', 'check_mk-agent' ]

- name: Install the required packages (Debian)
  apt: name="{{ item }}" state=present
  with_items:
    - xinetd
    - "{{ check_mk_agent_package }}"
  when: ansible_os_family == 'Debian'
  notify: restart xinetd
  tags: [ 'package', 'check_mk-agent' ]

######################################################################
# Configuration
######################################################################

# - name: Agregate Check_MK servers IP in a variable
#   debug:

- name: Enable Check_MK in Xinetd
  lineinfile: dest="{{ check_mk_agent_xinetd_conf }}"
              line="\1only_from\2= {{ check_mk_agent_servers | join(' ') }}"
              regexp="^(\s*)#?\s*only_from(\s*)="
              backrefs=yes
  notify: restart xinetd
  tags: [ 'configuration', 'package', 'check_mk-agent' ]

- name: Restrict allowed servers in Xinetd
  lineinfile: dest="{{ check_mk_agent_xinetd_conf }}"
              line="\1disable\2= no"
              regexp="^(\s*)disable(\s*)=\s*yes"
              backrefs=yes
  notify: restart xinetd
  tags: [ 'configuration', 'package', 'check_mk-agent' ]

- name: Enable Xinetd service
  service: name=xinetd enabled=yes
  tags: [ 'configuration', 'package', 'check_mk-agent' ]

- name: Make sure Xinetd is running
  service: name=xinetd state=started
  tags: [ 'configuration', 'package', 'check_mk-agent' ]

######################################################################
# Configure Check MK server
######################################################################

# FIXME TODO
# - name: Add this agent to each Check_MK servers
#   lineinfile:
#     delegate_to: "{{ item }}"
#     with_items: "{{ groups['{{ check_mk_agent_servers_group }}'] }}"
#   tags: [ 'configuration', 'check_mk-agent' ]

# FIXME TODO: This will be possible when Ansible is fixed in order to let
# delegate_to accept with_dict loops.
# - name: Add this agent to each servers
#   delegate_to: "{{ item.key }}"
#   lineinfile:
#     dest: "{{ item.value.hosts_file }}"
#     line: "'{{ ansible_hostname }}',"
#     insertbefore: "^]$"
#   with_dict: check_mk_agent_servers

# vim: ft=ansible
